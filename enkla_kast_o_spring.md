Kast och spring övningar
========================

      o |                 x

tre personer, kast med försvar, spring till andra sidan efter pass


            p
            .
              .
      x > . . . .     o

två och två, ryck mot, vänd och få pass med, tillbaka och den andra rycker


      x > . . . .
                .
                .
                .
      o         p

ett led med disc och ett utan, ryck, pass i magen


      2:a                 1:a
      o >         p . . . < x 
        . . . p                   

två led med disc, ryck rakt mot disc, pass i magen


      x > . . . . . . . po
      (1)
      
      
      
      O  . . . . . .  < X
                       (2)

Två led, en disc, pass diagonalt


spela två mot två, räkning till 6, träna på att rycka mot disc, välja ett håll, lägga även om tight. sedan give'n'go. lägg till en tredje som rycker om det inte går efter två försök.


      x      x
      
      
      
      x      X

Fyra personer står som koner. En anfallare och en försvarare i mitten. Går att köra två mot två.


Legend
------

Förklaring av symbolerna:

      o = dischållare
      x = spelare (anfallare)
      | = försvarare
      p = mottagning av pass
      

