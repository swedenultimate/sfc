Stenugnsunds Frisbee Club
========================

Material kring spelsystem och övningar.

Övningar:

 * [Kast och spring övningar](enkla_kast_o_spring.md)



Övrigt
-----
* [Hur man redigerar filer](https://help.github.com/articles/editing-files-in-your-repository/)
* [Hur man redigerar filer (del två)](https://help.github.com/articles/github-flavored-markdown/)
* [Ultimateregler på Svenska](http://ultimateregler.github.io)
